#!/bin/bash

######################
# Server file. Starts a server and records information in directory.
######################

dir=`pwd`
exptid=`date +%b%d-%H:%M`
toplogdir=$dir/logs/server-$exptid
pydir=$dir/src
distrf=$dir/distr0

server_ip="172.31.23.149"
stop_time=180 # seconds

for qsize in 83; do
    logdir=$toplogdir/run-$qsize
    echo "Log directory: $logdir"
    python $pydir/server.py \
        --log $logdir \
        --distr $distrf \
        --server $server_ip \
        --stop $stop_time
done
