#!/bin/bash

dir=`pwd`
exptid=`date +%b%d-%H:%M`
toplogdir=$dir/logs/$exptid
pydir=$dir/src
distrf=$dir/distr0

server_ip="172.31.23.149"
bwnet=10 # Gbps
delay=0.1 # default
iface=eth0

# adjust queue size and repeatedly save
for qsize in 50; do
    ./queue_script.sh $iface $qsize $delay
    logdir=$toplogdir/run-$qsize
    echo "Log directory: $logdir"
    python $pydir/buffersizing.py --bw-host $(( 10*$bwnet )) \
        --bw-net $bwnet \
        --delay $delay \
        --log $logdir \
        --distr $distrf \
        --qsize $qsize \
        --server $server_ip \
        --iface $iface
    
    echo "Running plot queue script."
    python $pydir/plot_queue.py -f $logdir/qlen.txt -o $dir/q.png
    echo "Running plot tcp probe script."
    python $pydir/plot_tcpprobe.py -f $logdir/tcp_probe.txt -o $dir/cwnd.png --histogram
done
