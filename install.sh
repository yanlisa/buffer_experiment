#!/bin/bash

# Install necessary packages
sudo apt-get update
sudo apt-get install git gcc g++ make
sudo apt-get install lib32z1 # for iperf3 linking
sudo apt-get install python-pip python-dev build-essential
sudo apt-get install imagemagick
wget http://downloads.es.net/pub/iperf/iperf-3.0.11.tar.gz
tar -xvf iperf-3.0.11.tar.gz
rm iperf-3.0.11.tar.gz
sudo apt-get install uuid-dev
cd iperf-3.0.11
./configure; make; sudo make install
cd ..
sudo apt-get install python-matplotlib
sudo pip install termcolor

# Get the correct git repo with src code.
