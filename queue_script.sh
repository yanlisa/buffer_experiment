#!/bin/bash

# defaults
iface=eth0
qsize=50
delay=0
bw=1000 # Mbps

if [ $# -ne 0 ] ; then
    # echo "Using arguments 1 $1, 2 $2, 3 $3, 4 $4";
    iface=$1;
    qsize=$2;
    delay=$3;
    bw=$4;
fi

if [ $bw -eq -1 ] ; then
    echo "Bandwidth limit not specified, so make very large";
    bw=1000;
fi

echo "Setting up qdisc for the first time."
echo "iface: $iface, qsize: $qsize, delay: $delay, bw: $bw"

echo "Deleting initial tree."
tc qdisc del dev $iface root # deletes entire tree

echo "Setting up new tree."
# tc qdisc add dev $iface handle 1: root htb default 11
# tc class add dev $iface parent 1: classid 1:1 htb rate 1000Mbps # root
# tc class add dev $iface parent 1:1 classid 1:11 htb rate 1000Mbps # parent
# tc qdisc add dev $iface parent 1:11 handle 10: netem limit $qsize
# tc qdisc add dev $iface parent 1:11 handle 10: netem limit $qsize
# tc qdisc add dev $iface parent 1:1 handle 10: netem limit $qsize
# tc qdisc change dev $iface handle 1: netem limit $qsize
 
######## By itself sets a limit on the queue. Everything is dropped though...
# tc qdisc add dev $iface root netem limit $qsize delay ${delay}ms

####### Default all goes to a particular class
tc qdisc add dev $iface handle 1: root htb default 1

# Bandwidth add
tc class add dev $iface parent 1: classid 1:1 htb rate ${bw}Mbit; 

# Delay and queue size add
tc qdisc add dev $iface parent 1:1 handle 10: netem delay $(( 2 * delay ))ms limit $qsize
# tc qdisc change dev $iface parent 1:1 handle 10: netem limit $qsize # merged into previous lien
# tc qdisc change dev $iface parent 1:11 handle 10: netem delay ${delay}ms

echo "Finished setting up qdisc."
    tc class show dev $iface
tc qdisc show dev $iface
