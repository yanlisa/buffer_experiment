#!/usr/bin/python

import subprocess
from subprocess import Popen, PIPE
from time import sleep, time
from multiprocessing import Process
import termcolor as T
from argparse import ArgumentParser

import sys
import os
from monitor import monitor_qlen
from helper import stdev

DEFAULT_PKT_SIZE = 1500 # bytes
DEFAULT_SERVER_PORT = 5001

# Parse arguments

parser = ArgumentParser(description="Buffer sizing tests")
parser.add_argument('--bw-host', dest="bw_host", type=float, action="store",
                    help="Bandwidth of host links",
                    required=True) # not used currently

parser.add_argument('--bw-net', dest="bw_net", type=float, action="store",
                    help="Bandwidth of network link",
                    required=True) # not used currently

parser.add_argument('--delay', dest="delay", type=float,
                    help="Delay in milliseconds of host links",
                    default=87)

parser.add_argument('--log', dest="log", action="store",
                    help="Directory to store outputs",
                    default="results", required=True)

parser.add_argument('--distr', dest="distr_f", action="store",
                    help="Distribution of flows for host",
                    required=True)

parser.add_argument('--qsize', dest="qsize", action="store",
                    help="Buffer size of network interface in packets",
                    default=1000)

parser.add_argument('--server', dest="server_ip", action="store",
                    help="Server IP (fixed port %d)" % DEFAULT_SERVER_PORT,
                    required=True)

parser.add_argument('--iface', dest="iface", action="store",
                    help="Interface to monitor",
                    default="lo")

# Expt parameters
args = parser.parse_args()

# Make log directory
if not os.path.exists(args.log):
    os.makedirs(args.log)

def cprint(s, color, cr=True):
    """Print in color
       s: string to print
       color: color to use"""
    if cr:
        print T.colored(s, color)
    else:
        print T.colored(s, color),

def verify_latency():
    cmd_rtt = "ping %s -c 1" % (args.server_ip)
    print "Desired RTT: %.2f ms" % (2*args.delay)
    p = Popen(cmd_rtt, shell=True, stdout=PIPE)
    ping_message, err = p.communicate()
    str_offset = ping_message.find('time')
    rtt = ping_message[str_offset:]
    rtt = rtt.split(' ms')[0]
    rtt = rtt.split('=')[1]
    rtt = float(rtt)
    print '\n'.join(ping_message.split('\n')[:2])
    

def verify_bandwidth(): # do we need this?
    server_port_num = DEFAULT_SERVER_PORT
    cmd = "iperf3 -c %s -p %d -t 3" % \
        (args.server_ip, server_port_num)
    p = Popen(cmd, shell=True)
    print p.communicate()

def start_senders(distr):
    p_handles = []
    server_port_num = DEFAULT_SERVER_PORT
    for flow_type in distr:
        nflows, npackets, tcp_or_udp, pktsize = flow_type
        udp_str = ""
        if tcp_or_udp == "udp": 
            udp_str = "-u"
        if nflows == 0: continue
        cmd = "iperf3 -c %s -p %d -i1 -k %d -M %d -C %s -P %d %s> " \
               "%s/iperf_host_%s.txt" %  \
            (args.server_ip, server_port_num, npackets, \
             pktsize-40, "reno", nflows, udp_str, \
             args.log,'-'.join(map(str,flow_type)))
        p = Popen(cmd, shell=True)
        print "Sender (PID %d): %s" % (p.pid, cmd)
        p_handles.append(p)
        server_port_num += 1;
    return p_handles

def parse_distribution():
    "Load distribution file into a list."
    # Each line of distribution file. Default packet size: 1500
    #     npackets tcp_or_udp nflows <pktsize>
    # ex: 100 tcp 10, or 20 udp 1000 5
    print "Opening distribution file: %s" % args.distr_f
    f = open(args.distr_f, 'r')
    distr = []
    line = f.readline()
    while line:
        d = (line.split('\n')[0]).split(' ')
        if len(d) < 4: d.append('%d' % DEFAULT_PKT_SIZE)
        if len(d) != 4:
            print "Ignoring line: %s" % line
        else:
            distr.append([int(d[0]), # nflows
                          int(d[1]), # npackets
                          d[2], # tcp_or_udp
                          int(d[3])]) # pktsize

        line = f.readline()
    return distr

def start_tcpprobe():
    "Install tcp_probe module and dump to file"
    os.system("rmmod tcp_probe 2>/dev/null; modprobe tcp_probe;")
    Popen("cat /proc/net/tcpprobe > %s/tcp_probe.txt" %
          args.log, shell=True)

def stop_tcpprobe():
    os.system("killall -9 cat; rmmod tcp_probe &>/dev/null;")

def start_monitor(iface):
    monitor = Process(target=monitor_qlen,
                      args=(iface, 0.01, '%s/qlen.txt' % args.log))
    monitor.start()
    return monitor

def set_q(iface, q):
    "Change queue size limit of interface"
    cmd = ("tc qdisc change dev %s parent 1:11 "
           "handle 10: netem limit %s" % (iface, q))
    # cmd = ("tc qdisc change dev %s root netem limit %s" % (iface, q))
    #os.system(cmd)
    print "set_q cmd: %s" % cmd
    subprocess.check_output(cmd, shell=True)

def set_delay(iface, delay):
    cmd = ("tc qdisc change dev %s root netem delay %sms" % (iface, delay))
    print "set_delay cmd: %s" % cmd

def reset_delay(iface):
    cmd = ("tc qdisc del dev %s root" % iface)
    print "reset_delay cmd: %s" % cmd

def main():
    "Create network and run Buffer Sizing experiment"
    start = time()
    verify_bandwidth()

    distr = parse_distribution()

    start_tcpprobe()
    print "interface:", args.iface
    # set_q(iface=args.iface, q=args.qsize)
    # set_delay(iface=args.iface, delay=args.delay)
    verify_latency()
    monitor = start_monitor(args.iface)

    cprint("Starting experiment", "green")

    p_send = start_senders(distr)
    for p in p_send:
        stdin, stdout = p.communicate()
        print "iperf PID %d has ended with code %d" % (p.pid, p.returncode)
    print "Finished iperfs."
    monitor.terminate()

    # Popen("killall -9 top bwm-ng tcpdump cat mnexec", shell=True).wait()
    os.system('killall -9 iperf3')
    stop_tcpprobe()
    end = time()
    cprint("Experiment took %.3f seconds" % (end - start), "yellow")

if __name__ == '__main__':
    try:
        main()
    except:
        print "-"*80
        print "Caught exception.  Cleaning up..."
        print "-"*80
        import traceback
        traceback.print_exc()
        os.system("killall -9 top bwm-ng tcpdump cat mnexec iperf; mn -c")

