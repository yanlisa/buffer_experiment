#!/usr/bin/python

import subprocess
from subprocess import Popen, PIPE
from time import sleep, time
from multiprocessing import Process
import termcolor as T
from argparse import ArgumentParser

import sys
import os
from monitor import monitor_qlen
from helper import stdev

DEFAULT_PKT_SIZE = 1500 # bytes
DEFAULT_SERVER_PORT = 5001
DEFAULT_STOP = 3600 # seconds

# Parse arguments

parser = ArgumentParser(description="Buffer sizing tests")
parser.add_argument('--log', dest="log", action="store",
                    help="Directory to store outputs",
                    default="results", required=True)

parser.add_argument('--distr', dest="distr_f", action="store",
                    help="Distribution of flows for host",
                    required=True)

parser.add_argument('--server', dest="server_ip", action="store",
                    help="Server IP (fixed port %d)" % DEFAULT_SERVER_PORT,
                    required=True)

parser.add_argument('--stop', dest="stop", type=int,
                    help="Time to run server for. Default is %d" % DEFAULT_STOP,
                    default=DEFAULT_STOP)

# Expt parameters
args = parser.parse_args()

# Make log directory
if not os.path.exists(args.log):
    os.makedirs(args.log)

def cprint(s, color, cr=True):
    """Print in color
       s: string to print
       color: color to use"""
    if cr:
        print T.colored(s, color)
    else:
        print T.colored(s, color),


def start_receiver(distr):
    # If this errors, check if server already listening on port:
    # sudo netstat -tulpn
    p_handles = []
    for offset_num in range(len(distr)):
        cmd = 'iperf3 -s -p %d' %  (DEFAULT_SERVER_PORT + offset_num)
        server_f = open('%s/iperf_server.txt' % (args.log), 'w')
        p = Popen(cmd, shell=True,
                stdout=server_f, stderr=subprocess.STDOUT)
        print "Receiver (PID %d): %s" % (p.pid, cmd)
        p_handles.append(p)
    return p_handles

def parse_distribution():
    "Load distribution file into a list."
    # Each line of distribution file. Default packet size: 1500
    #     npackets tcp_or_udp nflows <pktsize>
    # ex: 100 tcp 10, or 20 udp 1000 5
    print "Opening distribution file: %s" % args.distr_f
    f = open(args.distr_f, 'r')
    distr = []
    line = f.readline()
    while line:
        d = (line.split('\n')[0]).split(' ')
        if len(d) < 4: d.append('%d' % DEFAULT_PKT_SIZE)
        if len(d) != 4:
            print "Ignoring line: %s" % line
        else:
            distr.append([int(d[0]), # nflows
                          int(d[1]), # npackets
                          d[2], # tcp_or_udp
                          int(d[3])]) # pktsize

        line = f.readline()
    return distr

def main():
    "Run server."

    distr = parse_distribution()
    p_recv = start_receiver(distr)

    sleep(args.stop)
    print "Reached default stop time.  Killing server."
    for p in p_recv:
        print "Killing server (PID %d)." % (p.pid)
        p.kill()
    print "Finished iperfs."

    os.system('killall -9 iperf3')

if __name__ == '__main__':
    try:
        main()
    except:
        print "-"*80
        print "Caught exception.  Cleaning up..."
        print "-"*80
        import traceback
        traceback.print_exc()
        os.system("killall -9 top bwm-ng tcpdump cat mnexec iperf; mn -c")

