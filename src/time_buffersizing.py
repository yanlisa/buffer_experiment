#!/usr/bin/python

import subprocess
from subprocess import Popen, PIPE
from time import sleep, time
from multiprocessing import Process
import termcolor as T
from argparse import ArgumentParser

import sys
import os
from monitor import monitor_qlen
from helper import stdev

DEFAULT_PKT_SIZE = 1500 # bytes
TCP_HEADER=40
TARGET_UTIL_FRACTION = 0.98
SERVER_PORT = 5001

# Parse arguments

parser = ArgumentParser(description="Buffer sizing tests")
parser.add_argument('--bw-host', '-B',
                    dest="bw_host",
                    type=float,
                    action="store",
                    help="Bandwidth of host links",
                    required=True)

parser.add_argument('--bw-net', '-b',
                    dest="bw_net",
                    type=float,
                    action="store",
                    help="Bandwidth of network link",
                    required=True)

parser.add_argument('--delay',
                    dest="delay",
                    type=float,
                    help="Delay in milliseconds of host links",
                    default=87)

parser.add_argument('--log',
                    dest="log",
                    action="store",
                    help="Directory to store outputs",
                    default="results",
                    required=True)

parser.add_argument('--nflows',
                    dest="nflows",
                    action="store",
                    type=int,
                    help="Number of flows per host (for TCP)",
                    required=True)

parser.add_argument('--maxq',
                    dest="maxq",
                    action="store",
                    help="Max buffer size of network interface in packets",
                    default=1000)

parser.add_argument('--cong',
                    dest="cong",
                    help="Congestion control algorithm to use",
                    default="reno")

parser.add_argument('--target',
                    dest="target",
                    help="Target utilisation",
                    type=float,
                    default=TARGET_UTIL_FRACTION)

parser.add_argument('--iperf',
                    dest="iperf",
                    help="Path to custom iperf, or simply iperf3",
                    required=True)

parser.add_argument('--t',
                    type=int, # seconds
                    dest="runtime",
                    help="Runtime. If specified, no queue adjustment.",
                    default=-1)

parser.add_argument('--q', 
                    type=int, # packets
                    dest="qsize",
                    help="Starting queue size. If unspecified, bdp.",
                    default=-1)

parser.add_argument('--server', dest="server_ip", action="store",
                    help="Server IP (fixed port %d)" % SERVER_PORT,
                    required=True)

parser.add_argument('--iface', dest="iface", action="store",
                    help="Interface to monitor",
                    default="lo")

# Expt parameters
args = parser.parse_args()

CUSTOM_IPERF_PATH = args.iperf
if CUSTOM_IPERF_PATH != "iperf3":
    print "Using a custom iperf path other than iperf3, so verifying path: %s" \
        % CUSTOM_IPERF_PATH
    assert(os.path.exists(CUSTOM_IPERF_PATH))

if not os.path.exists(args.log):
    os.makedirs(args.log)


def cmd_iperf_server(CUSTOM_IPERF_PATH, port, output_dir="", f_prefix="iperf_server"):
    iperf_cmd = "%s -s -V -p %s" % (CUSTOM_IPERF_PATH, port)
    if output_dir:
        iperf_cmd = "%s > %s/%s.txt" % (iperf_cmd, output_dir, f_prefix)
    return iperf_cmd

def cmd_iperf_host(CUSTOM_IPERF_PATH, server_ip, server_port,
                   t, cong, output_dir="", f_prefix="",
                   nflows=1):
    "Returns a list of commands."
    interval=1
    iperf_cmd_list = []
    iperf_cmd = "%s -c %s -p %s -t %d -i%d" % \
        (CUSTOM_IPERF_PATH, server_ip, server_port, t, interval)
    if CUSTOM_IPERF_PATH == "iperf3":
        output_cmd = ""
        if output_dir and f_prefix:
            output_cmd = "> %s/%s.txt" % (output_dir, f_prefix)
        iperf_cmd_list.append("%s -M %d -C %s -P %s %s" % \
            (iperf_cmd, DEFAULT_PKT_SIZE - TCP_HEADER,
             cong, nflows, output_cmd))
    else:
        for flow in range(nflows):
            output_cmd = ""
            if output_dir and f_prefix:
                output_cmd = "> %s/%s_%d.txt" % (output_dir, f_prefix, flow)
            iperf_cmd_list.append("%s -yc -M %d -Z %s %s" % \
                (iperf_cmd, DEFAULT_PKT_SIZE - TCP_HEADER, cong, output_cmd))
    return iperf_cmd_list

def cprint(s, color, cr=True):
    """Print in color
       s: string to print
       color: color to use"""
    if cr:
        print T.colored(s, color)
    else:
        print T.colored(s, color),

def verify_latency():
    cmd_rtt = "ping %s -c 1" % (args.server_ip)
    print "Desired RTT: %.2f ms" % (2*args.delay)
    p = Popen(cmd_rtt, shell=True, stdout=PIPE)
    ping_message, err = p.communicate()
    str_offset = ping_message.find('time')
    rtt = ping_message[str_offset:]
    rtt = rtt.split(' ms')[0]
    rtt = rtt.split('=')[1]
    rtt = float(rtt)
    print '\n'.join(ping_message.split('\n')[:2])

def verify_bandwidth():
    seconds = 20
    cmd_host = cmd_iperf_host(CUSTOM_IPERF_PATH, args.server_ip, SERVER_PORT,
        seconds, args.cong)
    p = Popen(cmd_host, shell=True)
    print p.communicate()

def start_senders(t=3600):
    # Seconds to run iperf; keep this very high
    iperf_cmds = cmd_iperf_host(CUSTOM_IPERF_PATH, args.server_ip, SERVER_PORT,
        t, args.cong, output_dir=args.log, f_prefix="iperf_hostA",
        nflows=args.nflows)
    print "\n".join(iperf_cmds)
    clients = [Popen(cmd, shell=True) for cmd in iperf_cmds]
    return clients

def parse_distribution():
    "Load distribution file into a list."
    # Each line of distribution file. Default packet size: 1500
    #     npackets tcp_or_udp nflows <pktsize>
    # ex: 100 tcp 10, or 20 udp 1000 5
    print "Opening distribution file: %s" % args.distr_f
    f = open(args.distr_f, 'r')
    distr = []
    line = f.readline()
    while line:
        d = (line.split('\n')[0]).split(' ')
        if len(d) < 4: d.append('%d' % DEFAULT_PKT_SIZE)
        if len(d) != 4:
            print "Ignoring line: %s" % line
        else:
            distr.append([int(d[0]), # nflows
                          int(d[1]), # npackets
                          d[2], # tcp_or_udp
                          int(d[3])]) # pktsize

        line = f.readline()
    return distr

def start_tcpprobe():
    "Install tcp_probe module and dump to file"
    os.system("rmmod tcp_probe 2>/dev/null; modprobe tcp_probe full=1 port=%d;"% SERVER_PORT)
    Popen("cat /proc/net/tcpprobe > %s/tcp_probe.txt" %
          args.log, shell=True)

def stop_tcpprobe():
    os.system("killall -9 cat; rmmod tcp_probe &>/dev/null;")

def start_monitor(iface):
    monitor = Process(target=monitor_qlen,
                      args=(iface, 0.01, '%s/qlen.txt' % args.log))
    monitor.start()
    return monitor

def set_q(iface, q):
    "Change queue size limit of interface"
    cmd = ("tc qdisc change dev %s parent 1:11 "
           "handle 10: netem limit %s" % (iface, q))
    cmd = ("tc qdisc change dev %s root netem limit %s" % (iface, q))
    #os.system(cmd)
    print "\nSetting q=%d " % q,
    print "Before set_q."
    print subprocess.check_output("tc qdisc show dev %s" % (iface),shell=True)
    print "set_q cmd: %s" % cmd
    print subprocess.check_output(cmd, shell=True)
    print "Double checking queue size limit."
    print subprocess.check_output("tc qdisc show dev %s" % (iface),shell=True)

def set_delay(iface, delay):
    cmd = ("tc qdisc change dev %s root netem delay %sms" % (iface, delay))
    print "set_delay cmd: %s" % cmd

def reset_delay(iface):
    cmd = ("tc qdisc del dev %s root" % iface)
    print "reset_delay cmd: %s" % cmd

def main():
    "Create network and run Buffer Sizing experiment"
    start = time()
    verify_bandwidth()
    verify_latency()

    # distr = parse_distribution()

    start_tcpprobe()
    iface = args.iface
    print "interface:", iface
    # queue size and delay are set in queue_sizing.sh, so ignore these.
    # set_q(iface=iface, q=args.qsize)
    # set_delay(iface=iface, delay=args.delay)
    monitor = start_monitor(iface)

    cprint("Starting experiment", "green")

    t = args.runtime
    p_send = start_senders(t)
    print "Time trial: waiting for client to finish."
    remaining_time = t
    while remaining_time >= 0:
        print "Time remaining: %d seconds" % remaining_time
        sys.stdout.flush()
        remaining_time -= 10
        sleep(10)
    for p in p_send:
        stdin, stdout = p.communicate()
        print "iperf PID %d has ended with code %d" % (p.pid, p.returncode)
    print "Finished iperfs."
    monitor.terminate()

    # Popen("killall -9 top bwm-ng tcpdump cat mnexec", shell=True).wait()
    os.system('killall -9 iperf3')
    stop_tcpprobe()
    end = time()
    cprint("Experiment took %.3f seconds" % (end - start), "yellow")

if __name__ == '__main__':
    try:
        main()
    except:
        print "-"*80
        print "Caught exception.  Cleaning up..."
        print "-"*80
        import traceback
        traceback.print_exc()
        os.system("killall -9 top bwm-ng tcpdump cat mnexec iperf; mn -c")

