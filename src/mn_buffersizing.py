#!/usr/bin/python

"CS244 Assignment 2: Buffer Sizing"

from mininet.topo import Topo
from mininet.node import CPULimitedHost
from mininet.link import TCLink
from mininet.net import Mininet
from mininet.log import lg
from mininet.util import dumpNodeConnections
from mininet.cli import CLI

import subprocess
from subprocess import Popen, PIPE
from time import sleep, time
from multiprocessing import Process
import termcolor as T
from argparse import ArgumentParser

import sys
import os
from monitor import monitor_qlen
from helper import stdev

# Number of samples to skip for reference util calibration.
CALIBRATION_SKIP = 10

# Number of samples to grab for reference util calibration.
CALIBRATION_SAMPLES = 30

# Set the fraction of the link utilization that the measurement must exceed
# to be considered as having enough buffering.
TARGET_UTIL_FRACTION = 0.98

# Fraction of input bandwidth required to begin the experiment.
# At exactly 100%, the experiment may take awhile to start, or never start,
# because it effectively requires waiting for a measurement or link speed
# limiting error.
START_BW_FRACTION = 0.9

# Number of samples to take in get_rates() before returning.
NSAMPLES = 3

# Time to wait between samples, in seconds, as a float.
SAMPLE_PERIOD_SEC = 1.0

# Time to wait for first sample, in seconds, as a float.
SAMPLE_WAIT_SEC = 3.0

SERVER_PORT = 5001

DEFAULT_PKT_SIZE = 1500 # bytes
TCP_HEADER=40


def cprint(s, color, cr=True):
    """Print in color
       s: string to print
       color: color to use"""
    if cr:
        print T.colored(s, color)
    else:
        print T.colored(s, color),


# Parse arguments

parser = ArgumentParser(description="Buffer sizing tests")
parser.add_argument('--bw-host', '-B',
                    dest="bw_host",
                    type=float,
                    action="store",
                    help="Bandwidth of host links",
                    required=True)

parser.add_argument('--bw-net', '-b',
                    dest="bw_net",
                    type=float,
                    action="store",
                    help="Bandwidth of network link",
                    required=True)

parser.add_argument('--delay',
                    dest="delay",
                    type=float,
                    help="Delay in milliseconds of host links",
                    default=87)

parser.add_argument('--log',
                    dest="log",
                    action="store",
                    help="Directory to store outputs",
                    default="results",
                    required=True)

parser.add_argument('--nflows',
                    dest="nflows",
                    action="store",
                    type=int,
                    help="Number of flows per host (for TCP)",
                    required=True)

parser.add_argument('--maxq',
                    dest="maxq",
                    action="store",
                    help="Max buffer size of network interface in packets",
                    default=1000)

parser.add_argument('--cong',
                    dest="cong",
                    help="Congestion control algorithm to use",
                    default="reno")

parser.add_argument('--target',
                    dest="target",
                    help="Target utilisation",
                    type=float,
                    default=TARGET_UTIL_FRACTION)

parser.add_argument('--iperf',
                    dest="iperf",
                    help="Path to custom iperf, or simply iperf3",
                    required=True)

parser.add_argument('--t',
                    type=int, # seconds
                    dest="runtime",
                    help="Runtime. If specified, no queue adjustment.",
                    default=-1)

parser.add_argument('--q', 
                    type=int, # packets
                    dest="qsize",
                    help="Starting queue size. If unspecified, bdp.",
                    default=-1)

# Expt parameters
args = parser.parse_args()

CUSTOM_IPERF_PATH = args.iperf
if CUSTOM_IPERF_PATH != "iperf3":
    print "Using a custom iperf path other than iperf3, so verifying path: %s" \
        % CUSTOM_IPERF_PATH
    assert(os.path.exists(CUSTOM_IPERF_PATH))

if not os.path.exists(args.log):
    os.makedirs(args.log)

#lg.setLogLevel('info')
lg.setLogLevel('debug')

def cmd_iperf_server(CUSTOM_IPERF_PATH, port, output_dir="", f_prefix="iperf_server"):
    iperf_cmd = "%s -s -p %s -V" % (CUSTOM_IPERF_PATH, port)
    if output_dir:
        iperf_cmd = "%s &> %s/%s.txt" % (iperf_cmd, output_dir, f_prefix)
    return iperf_cmd

def cmd_iperf_host(CUSTOM_IPERF_PATH, server_ip, server_port,
                   t, cong, output_dir="", f_prefix="",
                   nflows=1):
    "Returns a list of commands."
    interval=1
    iperf_cmd_list = []
    iperf_cmd = "%s -c %s -p %s -t %d -i%d" % \
        (CUSTOM_IPERF_PATH, server_ip, server_port, t, interval)
    if CUSTOM_IPERF_PATH == "iperf3":
        output_cmd = ""
        if output_dir and f_prefix:
            output_cmd = "> %s/%s.txt" % (output_dir, f_prefix)
        iperf_cmd_list.append("%s -M %d -C %s -P %s %s" % \
            (iperf_cmd, DEFAULT_PKT_SIZE - TCP_HEADER,
             cong, nflows, output_cmd))
    else:
        for flow in range(nflows):
            output_cmd = ""
            if output_dir and f_prefix:
                output_cmd = "> %s/%s_%d.txt" % (output_dir, f_prefix, flow)
            iperf_cmd_list.append("%s -yc -M %d -Z %s %s" % \
                (iperf_cmd, DEFAULT_PKT_SIZE - TCP_HEADER, cong, output_cmd))
    return iperf_cmd_list

# Topology to be instantiated in Mininet
class StarTopo(Topo):
    "Star topology for Buffer Sizing experiment"

    def build(self, n=2, cpu=None, bw_host=None, bw_net=None,
                 delay=None, maxq=None):
        # Add default members to class.
        self.n = n
        self.cpu = cpu
        self.bw_host = bw_host
        self.bw_net = bw_net
        self.delay = delay
        self.maxq = maxq
        self.create_topology()

    def create_topology(self):
        hA = self.addHost('A')
        hC = self.addHost('C') # A-C, B-C bottleneck
        s0 = self.addSwitch('s0')

        perLinkDelayStr = str(args.delay/2) + 'ms' # two-link connection 
        # do-sweep sets queue size to RTT*C, so no need to do it here
        self.addLink(s0, hC, bw=self.bw_net, delay=perLinkDelayStr, \
                    max_queue_size=self.maxq) # bottleneck network link

        self.addLink(hA, s0, bw=self.bw_host, delay=perLinkDelayStr) # hA to s0 

        pass

def start_tcpprobe():
    "Install tcp_probe module and dump to file"
    os.system("rmmod tcp_probe 2>/dev/null; modprobe tcp_probe port=%d full=1;" % SERVER_PORT)
    Popen("cat /proc/net/tcpprobe > %s/tcp_probe.txt" %
          args.log, shell=True)

def stop_tcpprobe():
    os.system("killall -9 cat; rmmod tcp_probe &>/dev/null;")

def count_connections():
    "Count current connections in iperf output file"
    out = args.log + "/iperf_server.txt"
    lines = Popen("grep connected %s | wc -l" % out,
                  shell=True, stdout=PIPE).communicate()[0]
    return int(lines)

def set_q(iface, q): # q is number of packets.
    "Change queue size limit of interface. NOTE: Need delay again, because" \
    " resetting the tc qdisc."
    print "\nSetting q=%d " % q,
    print "Before set_q."
    print subprocess.check_output("tc qdisc show dev %s" % (iface),shell=True)
    cmd = ("tc qdisc change dev %s parent 5:1 "
           "handle 10: netem limit %s delay %sms" % (iface, q, args.delay/2))
    print "cmd", cmd
    #os.system(cmd)
    print subprocess.check_output(cmd,shell=True)
    print "Double checking queue size limit."
    print subprocess.check_output("tc qdisc show dev %s" % (iface),shell=True)
    print subprocess.check_output("tc class show dev %s" % (iface),shell=True)

def set_speed(iface, spd):
    "Change htb maximum rate for interface"
    cmd = ("tc class change dev %s parent 5:0 classid 5:1 "
           "htb rate %s burst 15k" % (iface, spd))
    os.system(cmd)

def get_txbytes(iface):
    f = open('/proc/net/dev', 'r')
    lines = f.readlines()
    for line in lines:
        if iface in line:
            break
    f.close()
    if not line:
        raise Exception("could not find iface %s in /proc/net/dev:%s" %
                        (iface, lines))
    # Extract TX bytes from:
    #Inter-|   Receive                                                |  Transmit
    # face |bytes    packets errs drop fifo frame compressed multicast|bytes    packets errs drop fifo colls carrier compressed
    # lo: 6175728   53444    0    0    0     0          0         0  6175728   53444    0    0    0     0       0          0
    return float(line.split()[9])

def get_rates(iface, nsamples=NSAMPLES, period=SAMPLE_PERIOD_SEC,
              wait=SAMPLE_WAIT_SEC):
    """Returns the interface @iface's current utilization in Mb/s.  It
    returns @nsamples samples, and each sample is the average
    utilization measured over @period time.  Before measuring it waits
    for @wait seconds to 'warm up'."""
    # Returning nsamples requires one extra to start the timer.
    nsamples += 1
    last_time = 0
    last_txbytes = 0
    ret = []
    sleep(wait)
    while nsamples:
        nsamples -= 1
        txbytes = get_txbytes(iface)
        now = time()
        elapsed = now - last_time
        #if last_time:
        #    print "elapsed: %0.4f" % (now - last_time)
        last_time = now
        # Get rate in Mbps; correct for elapsed time.
        rate = (txbytes - last_txbytes) * 8.0 / 1e6 / elapsed
        if last_txbytes != 0:
            # Wait for 1 second sample
            ret.append(rate)
        last_txbytes = txbytes
        print '.',
        sys.stdout.flush()
        sleep(period)
    return ret

def avg(s):
    "Compute average of list or string of values"
    if ',' in s:
        lst = [float(f) for f in s.split(',')]
    elif type(s) == str:
        lst = [float(s)]
    elif type(s) == list:
        lst = s
    return sum(lst)/len(lst)

def median(l):
    "Compute median from an unsorted list of values"
    s = sorted(l)
    if len(s) % 2 == 1:
        return s[(len(l) + 1) / 2 - 1]
    else:
        lower = s[len(l) / 2 - 1]
        upper = s[len(l) / 2]
        return float(lower + upper) / 2

def format_floats(lst):
    "Format list of floats to three decimal places"
    return ', '.join(['%.3f' % f for f in lst])

def ok(fraction):
    "Fraction is OK if it is >= args.target"
    return fraction >= args.target

def format_fraction(fraction):
    "Format and colorize fraction"
    if ok(fraction):
        return T.colored('%.3f' % fraction, 'green')
    return T.colored('%.3f' % fraction, 'red', attrs=["bold"])

def ramp_up(iface, net_debug=None):
    "Return monitor."
    bdp = args.bw_net * 2 * args.delay * 1000.0 / 8.0 / 1500.0
    print "bdp: %s (bw_net: %s, delay: %s)" % (bdp, args.bw_net, args.delay)
    nflows = args.nflows
    min_q, max_q = 1, int(bdp)
    start_qsize = max_q
    if args.qsize != -1:
        start_qsize = args.qsize

    monitor = Process(target=monitor_qlen,
                      args=(iface, 0.01, '%s/qlen.txt' %
                            (args.log)))
    monitor.start()


    set_speed(iface, "%.2fMbit" % args.bw_net)
    sys.stdout.flush()
    set_q(iface, start_qsize)
    print "Verifying latency again."
    verify_latency(net_debug)
    # if net_debug:
    #     print "verifying latency after setting queue."
    #     verify_latency(net_debug)
    return monitor

def do_sweep(iface):
    """Sweep queue length until we hit target utilization.
       We assume a monotonic relationship and use a binary
       search to find a value that yields the desired result"""
    bdp = args.bw_net * 2 * args.delay * 1000.0 / 8.0 / 1500.0
    nflows = args.nflows
    min_q, max_q = 1, int(bdp)

    # Wait till link is 100% utilised and train
    reference_rate = 0.0
    while reference_rate <= args.bw_net * START_BW_FRACTION:
        rates = get_rates(iface, nsamples=CALIBRATION_SAMPLES+CALIBRATION_SKIP)
        print "measured calibration rates: %s" % rates
        # Ignore first N; need to ramp up to full speed.
        rates = rates[CALIBRATION_SKIP:]
        reference_rate = median(rates)
        ru_max = max(rates)
        ru_stdev = stdev(rates)
        cprint ("Reference rate median: %.3f max: %.3f stdev: %.3f" %
                (reference_rate, ru_max, ru_stdev), 'blue')
        sys.stdout.flush()

    while abs(min_q - max_q) >= 2:
        mid = (min_q + max_q) / 2
        print "Trying q=%d  [%d,%d] " % (mid, min_q, max_q),
        sys.stdout.flush()

        # Note: this do_sweep function does a bunch of setup, so do
        # not recursively call do_sweep to do binary search.
        set_q(iface, mid)
        rates = get_rates(iface) # 3 samples
        utilization = median(rates)/reference_rate
        print "Utilization: %s", format_fraction(utilization)
        if ok(utilization):
            max_q = mid
        else:
            min_q = mid+1
        pass

    print "*** Minq for target: %d" % max_q
    return max_q

def verify_latency(net):
    print "Verifying latency..."
    # Ping once for each link.
    hC = net.get('C')

    cmd_rtt = "ping %s -c 1" % (hC.IP())
    print "Desired RTT: %.2f ms" % (2*args.delay)
    print "ping cmd", cmd_rtt
    hA = net.get('A')
    p = hA.popen(cmd_rtt, shell=True, stdout=PIPE)
    ping_message, err = p.communicate()
    print "ping error", err, "ping message", ping_message
    str_offset = ping_message.find('time')
    rtt = ping_message[str_offset:]
    rtt = rtt.split(' ms')[0]
    rtt = rtt.split('=')[1]
    rtt = float(rtt)
    print '\n'.join(ping_message.split('\n')[:2])
    print "RTT from Host A to Host C is %s" % (rtt)
    pass
    
def verify_bandwidth(net):
    print "Verifying bandwidth of bottleneck link."
    hA, hC = net.get('A', 'C')
    s0 = net.get('s0')
    seconds = 10
    # -P 2: max connections 1, then close (server-side)
    max_connections = 1
    cmd_C = cmd_iperf_server(CUSTOM_IPERF_PATH, SERVER_PORT+10)
    cmd_host = cmd_iperf_host(CUSTOM_IPERF_PATH, hC.IP(), SERVER_PORT+10,
        seconds, args.cong)
    p_server = hC.popen(cmd_C, shell=True)

    print "Starting Host A to Host C for %d seconds." % seconds
    pA = hA.popen(cmd_host, shell=True, stdout=PIPE)

    print "Desired Bandwidth: %.2f Mbps" % (args.bw_net)
    # from do_sweep
    rates = get_rates('s0-eth1')
    reference_rate = median(rates)
    print "Verify bandwidth median: %.3f Mbps" % (reference_rate)
    iperf_outA, err = pA.communicate()
    p_server.terminate() # Kill server
    print iperf_outA
    pass

def start_receiver(net):
    hC = net.get('C')
    print "Starting iperf server..."
    cmd = cmd_iperf_server(CUSTOM_IPERF_PATH, SERVER_PORT)
    print "Server command:", cmd
    server_f = open("%s/%s.txt" % (args.log, "iperf_server"), 'w')
    server = hC.popen(cmd, shell=True, stdout=server_f, stderr=subprocess.STDOUT)
    return server

def start_senders(net, t=3600):
    # Seconds to run iperf; keep this very high
    hA = net.get('A')
    server = net.get('C')

    iperf_cmds = cmd_iperf_host(CUSTOM_IPERF_PATH, server.IP(), SERVER_PORT,
        t, args.cong, output_dir=args.log, f_prefix="iperf_hostA",
        nflows=args.nflows)
    print "\n".join(iperf_cmds)
    clients = [hA.popen(cmd, shell=True) for cmd in iperf_cmds]
    return clients

def main():
    "Create network and run Buffer Sizing experiment"

    start = time()
    # Reset to known state
    topo = StarTopo(bw_host=args.bw_host,
                    delay='%sms' % args.delay,
                    bw_net=args.bw_net, maxq=args.maxq)
    net = Mininet(topo=topo, host=CPULimitedHost, link=TCLink)
    net.start()
    dumpNodeConnections(net.hosts)
    net.pingAll()

    # Ping once to test latency/bandwidth.
    verify_latency(net)
    verify_bandwidth(net)
    cprint("Starting experiment", "green")

    t = args.runtime
    monitor = ramp_up(iface='s0-eth1', net_debug=net)
    start_tcpprobe()

    p_server = start_receiver(net)
    if t !=-1: # no queue adjustment, so senders run for a set duraiton
        p_clients = start_senders(net, t)
    else: # senders run until killed
        p_clients = start_senders(net)

    if t != -1:
        print "Time trial only; no queue adjustment."
        print "Waiting for client to finish."
        remaining_time = t
        while remaining_time >= 0:
            print "Time remaining: %d seconds" % remaining_time
            sys.stdout.flush()
            remaining_time -= 10
            sleep(10)
            
        for p in p_clients:
            stdin, stdout = p.communicate()
            print "iperf PID %d has ended with code %d" % (p.pid, p.returncode)
    else:
        print "Doing sweep; queue adjustment."
        # TODO: change the interface for which queue size is adjusted
        ret = do_sweep(iface='s0-eth1')
        total_flows = args.nflows

        # Store output.  It will be parsed by run.sh after the entire
        # sweep is completed.  Do not change this filename!
        output = "%d %s %.3f\n" % (total_flows, ret, ret * 1500.0)
        open("%s/result.txt" % args.log, "w").write(output)

    monitor.terminate()
    print "Killing server (PID %d)." % (p_server.pid)
    p_server.kill()
    end = time()
    cprint("Before killing tcp probe: %.3f seconds" % (end - start), "yellow")
    stop_tcpprobe()

    # Shut down iperf processes
    # os.system('killall -9 ' + CUSTOM_IPERF_PATH)

    # Popen("killall -9 top bwm-ng tcpdump cat mnexec", shell=True).wait()
    net.stop()
    end = time()
    cprint("Sweep took %.3f seconds" % (end - start), "yellow")

if __name__ == '__main__':
    try:
        main()
    except:
        print "-"*80
        print "Caught exception.  Cleaning up..."
        print "-"*80
        import traceback
        traceback.print_exc()
        os.system("killall -9 top bwm-ng tcpdump cat mnexec iperf; mn -c")

