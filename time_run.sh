#!/bin/bash
# Uses mininet and iperf, not iperf3.

dir=`pwd`
exptid=`date +%b%d-%H:%M`
toplogdir=$dir/logs/$exptid
pydir=$dir/src
distrf=$dir/distr0

server_ip="172.31.23.149"
bwnet=4 # Mbps
delay=350 # ms, one way (2*delay: RTT)
delay=0 # for now
iface=eth0
# iperf=~/iperf-patched/src/iperf
iperf=iperf3
runtime=150

# adjust queue size and repeatedly save
# for flows_per_host in 1 2 5 10 50 100 200 300 400; do
for flows_per_host in 1; do
    # for qsize in 100 83 73 63 53 43 33; do
    # for qsize in 23 13 10 5 1; do
    for qsize in 120; do
        ./queue_script.sh $iface $qsize $delay -1
        logdir=$toplogdir/q$qsize
        echo "Log directory: $logdir"
        python $pydir/time_buffersizing.py --bw-host $(( 10*$bwnet )) \
            --bw-net $bwnet \
            --delay $delay \
            --log $logdir \
            --nflows $flows_per_host \
            --iperf $iperf \
            --t $runtime \
            --server $server_ip \
            --iface $iface \
            --q $qsize
        
        echo "Running plot queue script."
        python $pydir/plot_queue.py -f $logdir/qlen.txt -o $logdir/q.png
        echo "Running plot tcp probe script."
        python $pydir/plot_tcpprobe.py -f $logdir/tcp_probe.txt -o $logdir/cwnd.png --histogram
        python $pydir/plot_tcpprobe.py -f $logdir/tcp_probe.txt -o $logdir/cwnd_pkt.png --histogram --packet
    done
done
